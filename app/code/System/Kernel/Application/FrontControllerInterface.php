<?php
/**
 * Application front controller responsible for dispatching application requests
 *
 */
namespace System\Kernel\Application;

interface FrontControllerInterface
{
    /**
     * Dispatch application action
     *
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function dispatch(RequestInterface $request);
}
