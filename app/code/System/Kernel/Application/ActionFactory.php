<?php
/**
 * Action Factory
 *
 */
namespace System\Kernel\Application;

class ActionFactory
{
    /**
     * @var \System\Kernel\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @param \System\Kernel\ObjectManagerInterface $objectManager
     */
    public function __construct(\System\Kernel\ObjectManagerInterface $objectManager)
    {
        $this->_objectManager = $objectManager;
    }

    /**
     * Create action
     *
     * @param string $actionName
     * @return ActionInterface
     * @throws \InvalidArgumentException
     */
    public function create($actionName)
    {
        if (!is_subclass_of($actionName, '\System\Kernel\Application\ActionInterface')) {
            throw new \InvalidArgumentException('Invalid action name provided');
        }
        
        return $this->_objectManager->create($actionName);
    }
}
