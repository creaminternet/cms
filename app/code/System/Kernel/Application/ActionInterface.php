<?php
/**
 * Application action
 *
 */
namespace System\Kernel\Application;

interface ActionInterface
{
    const FLAG_NO_DISPATCH = 'no-dispatch';

    const FLAG_NO_POST_DISPATCH = 'no-postDispatch';

    const FLAG_NO_DISPATCH_BLOCK_EVENT = 'no-beforeGenerateLayoutBlocksDispatch';

    const PARAM_NAME_BASE64_URL = 'r64';

    const PARAM_NAME_URL_ENCODED = 'uenc';

    /**
     * Dispatch request
     *
     * @return \System\Kernel\Controller\ResultInterface|ResponseInterface
     * @throws \System\Kernel\Exception\NotFoundException
     */
    public function execute();
}
