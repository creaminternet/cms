<?php
/**
 * Abstract redirect/forward action class
 *
 */
namespace System\Kernel\Application\Action;

use System\Kernel\Application\RequestInterface;
use System\Kernel\Application\ResponseInterface;

abstract class AbstractAction implements \System\Kernel\Application\ActionInterface
{
    /**
     * @var \System\Kernel\Application\RequestInterface
     */
    protected $_request;

    /**
     * @var \System\Kernel\Application\ResponseInterface
     */
    protected $_response;

    /**
     * @var \System\Kernel\Controller\Result\RedirectFactory
     */
    protected $_resultRedirectFactory;

    /**
     * @var \System\Kernel\Controller\ResultFactory
     */
    protected $_resultFactory;

    /**
     * @param \System\Kernel\Application\Action\Context $context
     */
    public function __construct(
        \System\Kernel\Application\Action\Context $context
    ) {
        $this->_request = $context->getRequest();
        $this->_response = $context->getResponse();
        $this->_resultRedirectFactory = $context->getResultRedirectFactory();
        $this->_resultFactory = $context->getResultFactory();
    }

    /**
     * Dispatch request
     *
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    abstract public function dispatch(RequestInterface $request);

    /**
     * Retrieve request object
     *
     * @return \System\Kernel\Application\RequestInterface
     */
    public function getRequest()
    {
        return $this->_request;
    }

    /**
     * Retrieve response object
     *
     * @return \System\Kernel\Application\ResponseInterface
     */
    public function getResponse()
    {
        return $this->_response;
    }
}
