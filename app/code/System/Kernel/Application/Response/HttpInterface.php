<?php

namespace System\Kernel\Application\Response;

interface HttpInterface extends \System\Kernel\Application\ResponseInterface
{
    /**
     * Set HTTP response code
     *
     * @param int $code
     * @return void
     */
    public function setHttpResponseCode($code);
}
