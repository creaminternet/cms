<?php

namespace System\Kernel\Application;

use System\Kernel\Application\Io\DirectoryList;
use System\Kernel\ApplicationInterface;
use System\Kernel\Autoload\AutoloadRegistry;
use System\Kernel\Autoload\Populator;
//use Magento\Framework\Component\ComponentRegistrar;
use System\Io\DriverPool;
use System\Kernel\Profiler;
use System\Kernel\Config\File\ConfigFilePool;

/**
 * Performs basic initialization root function: injects init parameters and creates object manager
 * Can create/run applications
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Bootstrap
{
    /**#@+
     * Possible errors that can be triggered by the bootstrap
     */
    const ERR_MAINTENANCE = 901;
    const ERR_IS_INSTALLED = 902;
    /**#@- */

    /**#@+
     * Initialization parameters that allow control bootstrap behavior of asserting maintenance mode or is installed
     *
     * Possible values:
     * - true -- set expectation that it is required
     * - false -- set expectation that is required not to
     * - null -- bypass the assertion completely
     *
     * If key is absent in the parameters array, the default behavior will be used
     * @see DEFAULT_REQUIRE_MAINTENANCE
     * @see DEFAULT_REQUIRE_IS_INSTALLED
     */
    const PARAM_REQUIRE_MAINTENANCE = 'MAGE_REQUIRE_MAINTENANCE';
    const PARAM_REQUIRE_IS_INSTALLED = 'MAGE_REQUIRE_IS_INSTALLED';
    /**#@- */

    /**#@+
     * Default behavior of bootstrap assertions
     */
    const DEFAULT_REQUIRE_MAINTENANCE = false;
    const DEFAULT_REQUIRE_IS_INSTALLED = true;
    /**#@- */

    /**
     * Initialization parameter for custom directory paths
     */
    const INIT_PARAM_FILESYSTEM_DIR_PATHS = 'MAGE_DIRS';

    /**
     * Initialization parameter for additional filesystem drivers
     */
    const INIT_PARAM_FILESYSTEM_DRIVERS = 'MAGE_FILESYSTEM_DRIVERS';

    /**
     * The initialization parameters (normally come from the $_SERVER)
     *
     * @var array
     */
    private $server;

    /**
     * Root directory
     *
     * @var string
     */
    private $rootDir;

    /**
     * Object manager
     *
     * @var \System\Kernel\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Maintenance mode manager
     *
     * @var \System\Kernek\Application\MaintenanceMode
     */
    private $maintenance;

    /**
     * Bootstrap-specific error code that may have been set in runtime
     *
     * @var int
     */
    private $errorCode = 0;

    /**
     * Attribute for creating object manager
     *
     * @var ObjectManagerFactory
     */
    private $factory;

    /**
     * Static method so that client code does not have to create Object Manager Factory every time Bootstrap is called
     *
     * @param string $rootDir
     * @param array $initParams
     * @param ObjectManagerFactory $factory
     * @return Bootstrap
     */
    public static function create($rootDir, array $initParams, ObjectManagerFactory $factory = null)
    {
        self::populateAutoloader($rootDir, $initParams);
        if ($factory === null) {
            $factory = self::createObjectManagerFactory($rootDir, $initParams);
        }
        return new self($factory, $rootDir, $initParams);
    }

    /**
     * Populates autoloader with mapping info
     *
     * @param string $rootDir
     * @param array $initParams
     * @return void
     */
    public static function populateAutoloader($rootDir, $initParams)
    {
        $dirList = self::createFilesystemDirectoryList($rootDir, $initParams);
        $autoload = AutoloadRegistry::getAutoloader();
        Populator::populateMappings($autoload, $dirList);
    }

    /**
     * Creates instance of object manager factory
     *
     * @param string $rootDir
     * @param array $initParams
     * @return ObjectManagerFactory
     */
    public static function createObjectManagerFactory($rootDir, array $initParams)
    {
        $dirList = self::createFilesystemDirectoryList($rootDir, $initParams);
        $driverPool = self::createFilesystemDriverPool($initParams);
        $configFilePool = self::createConfigFilePool();
        
        return new ObjectManagerFactory($dirList, $driverPool, $configFilePool);
    }

    /**
     * Creates instance of filesystem directory list
     *
     * @param string $rootDir
     * @param array $initParams
     * @return DirectoryList
     */
    public static function createFilesystemDirectoryList($rootDir, array $initParams)
    {
        $customDirs = [];
        if (isset($initParams[Bootstrap::INIT_PARAM_FILESYSTEM_DIR_PATHS])) {
            $customDirs = $initParams[Bootstrap::INIT_PARAM_FILESYSTEM_DIR_PATHS];
        }
        return new \System\Kernel\Application\Io\DirectoryList($rootDir, $customDirs);
    }

    /**
     * Creates instance of filesystem driver pool
     *
     * @param array $initParams
     * @return DriverPool
     */
    public static function createFilesystemDriverPool(array $initParams)
    {
        $extraDrivers = [];
        if (isset($initParams[Bootstrap::INIT_PARAM_FILESYSTEM_DRIVERS])) {
            $extraDrivers = $initParams[Bootstrap::INIT_PARAM_FILESYSTEM_DRIVERS];
        };

        return new DriverPool($extraDrivers);
    }
    
    /**
     * Creates instance of configuration files pool
     *
     * @return DriverPool
     */
    public static function createConfigFilePool()
    {
    	return new ConfigFilePool();
    }    

    /**
     * Constructor
     *
     * @param ObjectManagerFactory $factory
     * @param string $rootDir
     * @param array $initParams
     */
    public function __construct(ObjectManagerFactory $factory, $rootDir, array $initParams)
    {
        $this->factory = $factory;
        $this->rootDir = $rootDir;
        $this->server = $initParams;
    }

    /**
     * Gets the current parameters
     *
     * @return array
     */
    public function getParams()
    {
        return $this->server;
    }

    /**
     * Factory method for creating application instances
     *
     * @param string $type
     * @param array $arguments
     * @return \Systme\Kernel\ApplicationInterface
     * @throws \InvalidArgumentException
     */
    public function createApplication($type, $arguments = [])
    {
        try {
            $this->initObjectManager();
            $application = $this->objectManager->create($type, $arguments);
            if (!($application instanceof AppInterface)) {
                throw new \InvalidArgumentException("The provided class doesn't implement AppInterface: {$type}");
            }
            return $application;
        } catch (\Exception $e) {
            $this->terminate($e);
        }
    }

    /**
     * Runs an application
     *
     * @param \System\Kernel\ApplicationInterface $application
     * @return void
     */
    public function run(AppInterface $application)
    {
        try {
            try {
                \System\Kernel\Profiler::start('kernel');
                $this->initErrorHandler();
                $this->initObjectManager();
                $this->assertMaintenance();
                $this->assertInstalled();
                $response = $application->launch();
                $response->sendResponse();
                \System\Kernel\Profiler::stop('kernel');
            } catch (\Exception $e) {
                \System\Kernel\Profiler::stop('kernel');
                if (!$application->catchException($this, $e)) {
                    throw $e;
                }
            }
        } catch (\Exception $e) {
            $this->terminate($e);
        }
    }

    /**
     * Asserts maintenance mode
     *
     * @return void
     * @throws \Exception
     */
    protected function assertMaintenance()
    {
        $isExpected = $this->getIsExpected(self::PARAM_REQUIRE_MAINTENANCE, self::DEFAULT_REQUIRE_MAINTENANCE);
        if (null === $isExpected) {
            return;
        }
        $this->initObjectManager();
        /** @var \System\Kernel\Application\MaintenanceMode $maintenance */
        $this->maintenance = $this->objectManager->get('System\Kernel\Application\MaintenanceMode');
        $isOn = $this->maintenance->isOn(isset($this->server['REMOTE_ADDR']) ? $this->server['REMOTE_ADDR'] : '');
        if ($isOn && !$isExpected) {
            $this->errorCode = self::ERR_MAINTENANCE;
            throw new \Exception('Unable to proceed: the maintenance mode is enabled.');
        }
        if (!$isOn && $isExpected) {
            $this->errorCode = self::ERR_MAINTENANCE;
            throw new \Exception('Unable to proceed: the maintenance mode must be enabled first.');
        }
    }

    /**
     * Asserts whether application is installed
     *
     * @return void
     * @throws \Exception
     */
    protected function assertInstalled()
    {
        $isExpected = $this->getIsExpected(self::PARAM_REQUIRE_IS_INSTALLED, self::DEFAULT_REQUIRE_IS_INSTALLED);
        if (null === $isExpected) {
            return;
        }
        $this->initObjectManager();
        $isInstalled = $this->isInstalled();
        if (!$isInstalled && $isExpected) {
            $this->errorCode = self::ERR_IS_INSTALLED;
            throw new \Exception('Application is not installed yet.');
        }
        if ($isInstalled && !$isExpected) {
            $this->errorCode = self::ERR_IS_INSTALLED;
            throw new \Exception('Application is already installed.');
        }
    }

    /**
     * Analyze a key in the initialization parameters as "is expected" parameter
     *
     * If there is no such key, returns default value. Otherwise casts it to boolean, unless it is null
     *
     * @param string $key
     * @param bool $default
     * @return bool|null
     */
    private function getIsExpected($key, $default)
    {
        if (array_key_exists($key, $this->server)) {
            if (isset($this->server[$key])) {
                return (bool)(int)$this->server[$key];
            }
            return null;
        }
        return $default;
    }

    /**
     * Determines whether application is installed
     *
     * @return bool
     */
    private function isInstalled()
    {
        $this->initObjectManager();
        /** @var \System\Kernel\Application\DeploymentConfig $deploymentConfig */
        $deploymentConfig = $this->objectManager->get('System\Kernel\Application\DeploymentConfig');
        return $deploymentConfig->isAvailable();
    }

    /**
     * Gets the object manager instance
     *
     * @return \System\Kernel\ObjectManagerInterface
     */
    public function getObjectManager()
    {
        $this->initObjectManager();
        return $this->objectManager;
    }

    /**
     * Sets a custom error handler
     *
     * @return void
     */
    private function initErrorHandler()
    {
        $handler = new ErrorHandler();
        set_error_handler([$handler, 'handler']);
    }

    /**
     * Initializes object manager
     *
     * @return void
     */
    private function initObjectManager()
    {
        if (!$this->objectManager) {
            $this->objectManager = $this->factory->create($this->server);
            $this->maintenance = $this->objectManager->get('System\Kernel\Application\MaintenanceMode');
        }
    }

    /**
     * Getter for error code
     *
     * @return int
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * Checks whether developer mode is set in the initialization parameters
     *
     * @return bool
     */
    public function isDeveloperMode()
    {
        return isset($this->server[State::PARAM_MODE]) && $this->server[State::PARAM_MODE] == State::MODE_DEVELOPER;
    }

    /**
     * Display an exception and terminate program execution
     *
     * @param \Exception $e
     * @return void
     * @SuppressWarnings(PHPMD.ExitExpression)
     */
    protected function terminate(\Exception $e)
    {
        if ($this->isDeveloperMode()) {
            echo $e;
        } else {
            $message = "An error has happened during application run. See debug log for details.\n";
            try {
                if (!$this->objectManager) {
                    throw new \DomainException();
                }
                $this->objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            } catch (\Exception $e) {
                $message .= "Could not write error message to log. Please use developer mode to see the message.\n";
            }
            echo $message;
        }
        exit(1);
    }
}
