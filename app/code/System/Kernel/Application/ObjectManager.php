<?php

namespace System\Kernel\Application;

use System\Kernel\ObjectManager\FactoryInterface;

/**
 * A wrapper around object manager with workarounds to access it in client code
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ObjectManager extends \System\Kernel\ObjectManager\ObjectManager
{
    /**
     * @var ObjectManager
     */
    protected static $_instance;

    /**
     * Retrieve object manager
     *
     * @return ObjectManager
     * @throws \RuntimeException
     */
    public static function getInstance()
    {
        if (!self::$_instance instanceof \System\Kernel\ObjectManagerInterface) {
            throw new \RuntimeException('ObjectManager isn\'t initialized');
        }
        return self::$_instance;
    }

    /**
     * Set object manager instance
     *
     * @param \System\Kernel\ObjectManagerInterface $objectManager
     * @throws \LogicException
     * @return void
     */
    public static function setInstance(\System\Kernel\ObjectManagerInterface $objectManager)
    {
        self::$_instance = $objectManager;
    }

    /**
     * @param FactoryInterface $factory
     * @param \System\Kernel\ObjectManager\ConfigInterface $config
     * @param array $sharedInstances
     */
    public function __construct(
        FactoryInterface $factory,
        \System\Kernel\ObjectManager\ConfigInterface $config,
        array &$sharedInstances = []
    ) {
        parent::__construct($factory, $config, $sharedInstances);
        self::$_instance = $this;
    }
}
