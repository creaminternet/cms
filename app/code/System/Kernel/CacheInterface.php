<?php
/**
 * System cache model interface
 *
 */
namespace Sytem\Kernel;

interface CacheInterface
{
    /**
     * Get cache frontend API object
     *
     * @return \System\Kernel\Cache\FrontendInterface
     */
    public function getFrontend();

    /**
     * Load data from cache by the given identifier.
     *
     * @param  string $identifier
     * @return string
     */
    public function load($identifier);

    /**
     * Save data to cache.
     *
     * @param string $data
     * @param string $identifier
     * @param array $tags
     * @param int $lifeTime
     * @return bool
     */
    public function save($data, $identifier, $tags = [], $lifeTime = null);

    /**
     * Remove cached data by identifier
     *
     * @param string $identifier
     * @return bool
     */
    public function remove($identifier);

    /**
     * Clean cached data by specific tag
     *
     * @param array $tags
     * @return bool
     */
    public function clean($tags = []);
}
