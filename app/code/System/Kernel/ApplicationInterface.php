<?php

namespace System\Kernel;

interface ApplicationInterface
{
    /**
     * Launch application
     *
     * @return \System\Kerne\Application\ResponseInterface
     */
    public function launch();
}
