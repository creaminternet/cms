<?php

namespace System\Kernel\Autoload;

use System\Kernel\Autoload\AutoloadInterface;

/**
 * Registry to store a static member autoloader
 */
class AutoloadRegistry
{
    /**
     * @var AutoloadInterface
     */
    protected static $autoloader;

    /**
     * Registers the given autoloader as a static member
     *
     * @param AutoloaderInterface $newAutoloader
     * @return void
     */
    public static function registerAutoloader(AutoloadInterface $autoloader)
    {
        self::$autoloader = $autoloader;
    }

    /**
     * Returns the registered autoloader
     *
     * @throws \Exception
     * @return AutoloadInterface
     */
    public static function getAutoloader()
    {
        if (self::$autoloader !== null) {
            return self::$autoloader;
        } else {
            throw new \Exception('Autoloader is not registered, cannot be retrieved.');
        }
    }
}
