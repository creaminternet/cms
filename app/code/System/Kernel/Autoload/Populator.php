<?php

namespace System\Kernel\Autoload;

use System\Kernel\Application\Io\DirectoryList;
use System\Kernel\Autoload\AutoloadInterface;
use System\Io\FileResolver;

/**
 * Utility class for populating an autoloader with application-specific information for PSR-0 and PSR-4 mappings
 * and include-path contents
 */
class Populator
{
    /**
     * @param AutoloaderInterface $autoloader
     * @param DirectoryList $dirList
     * @return void
     */
    public static function populateMappings(AutoloadInterface $autoloader, DirectoryList $dirList)
    {
        $generationDir = $dirList->getPath(DirectoryList::GENERATION);
        $frameworkDir = $dirList->getPath(DirectoryList::LIB_INTERNAL);

        /** Required for Zend functionality */
        //FileResolver::addIncludePath($frameworkDir);

        /** Required for code generation to occur */
        //FileResolver::addIncludePath($generationDir);

        /** Required to autoload custom classes */
        $autoloader->addPsr0('', [$generationDir]);
    }
}
