<?php

namespace System\Io;

/**
 * A pool of stream wrappers
 */
class DriverPool
{
    /**#@+
     * Available driver types
     */
    const FILE = 'file';
    const HTTP = 'http';
    const HTTPS = 'https';
    const ZLIB = 'compress.zlib';
    /**#@- */

    /**
     * Supported types
     *
     * @var string[]
     */
    protected $types = [
        self::FILE => 'System\Io\Filesystem\Driver\File',
        self::HTTP => 'System\Io\Filesystem\Driver\Http',
        self::HTTPS => 'System\Io\Filesystem\Driver\Https',
        self::ZLIB => 'System\Io\Filesystem\Driver\Zlib',
    ];

    /**
     * The pool
     *
     * @var DriverInterface[]
     */
    private $pool = [];

    /**
     * Obtain extra types in constructor
     *
     * @param array $extraTypes
     * @throws \InvalidArgumentException
     */
    public function __construct($extraTypes = [])
    {
        foreach ($extraTypes as $code => $typeOrObject) {
            if (is_object($typeOrObject)) {
                $type = get_class($typeOrObject);
                $object = $typeOrObject;
            } else {
                $type = $typeOrObject;
                $object = false;
            }
            if (!is_subclass_of($type, '\System\Io\Filesystem\DriverInterface')) {
                throw new \InvalidArgumentException("The specified type '{$type}' does not implement DriverInterface.");
            }
            $this->types[$code] = $type;
            if ($object) {
                $this->pool[$code] = $typeOrObject;
            }
        }
    }

    /**
     * Gets a driver instance by code
     *
     * @param string $code
     * @return DriverInterface
     */
    public function getDriver($code)
    {
        if (!isset($this->types[$code])) {
            $code = self::FILE;
        }
        if (!isset($this->pool[$code])) {
            $class = $this->types[$code];
            $this->pool[$code] = new $class();
        }
        return $this->pool[$code];
    }
}
