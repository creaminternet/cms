<?php

namespace System\Exception;

class CouldNotDeleteException extends LocalizedException
{
}
