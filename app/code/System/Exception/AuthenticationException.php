<?php

namespace System\Exception;

class AuthenticationException extends LocalizedException
{
    const AUTHENTICATION_ERROR = 'An authentication error occurred.';
}
