<?php

namespace System\Exception;

/**
 * Mail exception
 */
class MailException extends LocalizedException
{
}
