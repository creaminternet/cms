<?php

namespace System\Exception;

/**
 * Class InvalidEmailOrPasswordException
 */
class InvalidEmailOrPasswordException extends AuthenticationException
{
    const INVALID_EMAIL_OR_PASSWORD = 'Invalid email or password';
}
