<?php

namespace System\Exception;

/**
 * Filesystem exception
 */
class FileSystemException extends LocalizedException
{
}
