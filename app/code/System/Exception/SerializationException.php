<?php

namespace System\Exception;

use System\Globalization\Phrase;

/**
 * Serialization Exception
 */
class SerializationException extends LocalizedException
{
    const DEFAULT_MESSAGE = 'Invalid type';
    const TYPE_MISMATCH = 'Invalid type for value: "%value". Expected Type: "%type".';

    /**
     * @param \System\Globalization\Phrase $phrase
     * @param \Exception $cause
     */
    public function __construct(Phrase $phrase = null, \Exception $cause = null)
    {
        if ($phrase === null) {
            $phrase = new Phrase(self::DEFAULT_MESSAGE);
        }
        parent::__construct($phrase, $cause);
    }
}
