<?php

namespace System\Exception;

class AuthorizationException extends LocalizedException
{
    const NOT_AUTHORIZED = 'Consumer is not authorized to access %resources';
}
