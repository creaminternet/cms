<?php

namespace System\Exception;

/**
 * Class EmailNotConfirmedException
 */
class EmailNotConfirmedException extends AuthenticationException
{
    const EMAIL_NOT_CONFIRMED = 'Email not confirmed';
}
