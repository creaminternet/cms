<?php

namespace System\Exception;

class CouldNotSaveException extends AbstractAggregateException
{
}
