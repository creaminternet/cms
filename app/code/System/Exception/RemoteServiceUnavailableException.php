<?php

namespace System\Exception;

class RemoteServiceUnavailableException extends AuthenticationException
{
}
