<?php

namespace System\Exception;

/**
 * Session exception
 */
class SessionException extends LocalizedException
{
}
