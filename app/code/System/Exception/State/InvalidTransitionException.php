<?php

namespace System\Exception\State;

use System\Exception\StateException;

class InvalidTransitionException extends StateException
{
}
