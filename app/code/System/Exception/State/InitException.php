<?php

namespace System\Exception\State;

use System\Exception\LocalizedException;

/**
 * An exception that indicates application initialization error
 */
class InitException extends LocalizedException
{
}
