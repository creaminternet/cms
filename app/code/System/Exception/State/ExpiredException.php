<?php

namespace System\Exception\State;

use System\Exception\StateException;

class ExpiredException extends StateException
{
}
