<?php

namespace System\Exception\State;

use System\Exception\AuthenticationException;

class UserLockedException extends AuthenticationException
{
}
