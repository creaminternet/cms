<?php

namespace System\Exception;

/**
 * State Exception
 */
class StateException extends LocalizedException
{
}
