<?php

namespace System\Exception;

class NotFoundException extends LocalizedException
{
}
