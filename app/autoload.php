<?php
/**
 * Register basic autoloader that uses include path
 *
 */
use System\Kernel\Autoload\AutoloadRegistry;
use System\Kernel\Autoload\ClassLoader;

/**
 * Shortcut constant for the root directory
 */
define('BP', dirname(__DIR__));

require_once BP .'/app/code/System/Kernel/Autoload/AutoloadInterface.php';
require_once BP .'/app/code/System/Kernel/Autoload/ClassLoader.php';

$loader = new ClassLoader();
$loader->addPsr4('', BP .'/app/code');
$loader->register();

AutoloadRegistry::registerAutoloader($loader);

// Sets default autoload mappings, may be overridden in Bootstrap::create
//\System\Kernel\Bootstrap::populateAutoloader(BP, []);
