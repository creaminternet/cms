<?php
/**
 * Environment initialization
 */
error_reporting(E_ALL);
ini_set('display_errors', 1);
umask(0);

/* PHP version validation */
if (version_compare(phpversion(), '5.5.0', '<') === true) {
    if (PHP_SAPI == 'cli') {
        echo 'Application supports PHP 5.5.0 or later. ';
    } else {
        echo <<<HTML
<div style="font:12px/1.35em arial, helvetica, sans-serif;">
    <p>Application supports PHP 5.5.0 or later.</p>
</div>
HTML;
    }
    exit(1);
}

require_once __DIR__ . '/autoload.php';

if (!empty($_SERVER['PROFILER'])) {
    \System\Kernel\Profiler::applyConfig($_SERVER['PROFILER'], BP, !empty($_REQUEST['isAjax']));
}
if (ini_get('date.timezone') == '') {
    date_default_timezone_set('UTC');
}